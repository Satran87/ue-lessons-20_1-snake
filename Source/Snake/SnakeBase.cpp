// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Engine/Engine.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovemendSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}


// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovemendSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int32 ElementsNum)
{
	for(int t=0;t<ElementsNum;++t)
	{
		auto num = SnakeElemnts.Num();
		auto xLocation = ElementSize*num;
		const FVector NewLocation(xLocation ,0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		int32 ElementIndex=SnakeElemnts.Add(NewSnakeElement);
		if(ElementIndex==0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	const float MovementSpeed = ElementSize;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		{
			MovementVector.X += MovementSpeed;
			break;
		};
	case EMovementDirection::DOWN:
		{
			MovementVector.X -= MovementSpeed;
			break;
		};
	case EMovementDirection::LEFT:
		{
			MovementVector.Y += MovementSpeed;
			break;
		};
	case EMovementDirection::RIGHT:
		{
			MovementVector.Y -= MovementSpeed;
			break;
		};
	default: ;
	}
	for(int t=SnakeElemnts.Num()-1;t>0;t--)
	{
		auto CurrentElement = SnakeElemnts[t];
		auto PrevElement = SnakeElemnts[t - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElemnts[0]->AddActorWorldOffset(MovementVector);
}

